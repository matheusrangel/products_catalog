class Product < ApplicationRecord
	has_and_belongs_to_many :categories
	has_many :reviews
	belongs_to :manufacturer

	validates :title, :description, :price, presence: true
	validates :price, numericality: { greater_than: 0 }
	validates :categories, presence: true

	scope :title_contains, -> (title) { where("title like ?", "%#{title}%") }
	scope :manufacturer, -> (manufacturer_id) { where manufacturer_id: manufacturer_id }
	
	def as_json(options={})
    options[:methods] = [:category_ids]
    super
  end

end
