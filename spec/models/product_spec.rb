require 'rails_helper'

RSpec.describe Product, type: :model do
  it 'deve ser valido' do
    expect(build(:product)).to be_valid
  end

  it 'deve ser invalido quando preço for 0 ou menor' do
    expect(build(:product, price: 0)).not_to be_valid
    expect(build(:product, price: -1)).not_to be_valid
  end

  it 'deve ser invalido se nao possuir algum atributo' do
    expect(build(:product, title: nil)).not_to be_valid
    expect(build(:product, description: nil)).not_to be_valid
    expect(build(:product, price: nil)).not_to be_valid
    expect(build(:product, :without_categories)).not_to be_valid
    expect(build(:product, :without_manufacturer)).not_to be_valid
  end
end
