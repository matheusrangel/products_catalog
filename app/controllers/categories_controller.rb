class CategoriesController < ApplicationController
  before_action :set_category, only: [:show]

  swagger_controller :categories, 'Categorias'

  swagger_api :index do
    summary 'Retorna categorias existentes'
    notes 'Este serviço busca por categorias'
  end

  # GET /categories
  def index
    @categories = Category.all

    render json: @categories
  end

  swagger_api :show do
    summary 'Retorna uma Categoria'
    notes 'Este serviço busca por uma Categoria através do Id'
    param :path, :id, :integer, :required, 'Id da Categoria'
  end

  # GET /categories/1
  def show
    render json: @category
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_category
      @category = Category.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def category_params
      params.require(:category).permit(:name, :description)
    end
end
