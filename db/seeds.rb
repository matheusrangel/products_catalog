# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

Manufacturer.create(name: 'Samsung', phone: '551188888888')
Manufacturer.create(name: 'Sony', phone: '551199999999')

categories = Category.create([
  { name: 'Eletrodomésticos', description: 'Geladeiras, Lava-roupas, Fogões' },
  { name: 'Casa', description: 'Produtos para sua casa' },
  { name: 'Televisões', description: 'Todas as marcas, pelos melhores preços!' } 
])

Product.create([
  { title: 'Geladeira', description: 'Frost-free', price: 2000.0, 
    manufacturer_id: 1, category_ids: [categories[0].id, categories[1].id] },
  { title: 'TV', description: '40 polegadas Full HD', price: 1850, 
    manufacturer_id: 2, category_ids: [categories[2].id] }
])

Review.create([
  { rate: 5, comment: 'Excelente!!!', date: DateTime.new(2018,7,28), product: Product.first },
  { rate: 1, comment: 'Não comprem! Produto Horrível!', date: DateTime.new(2018,7,28), 
    product: Product.first },
  { rate: 3, comment: 'Razoável', date: DateTime.new(2018,7,29), product: Product.last }
])
