FactoryGirl.define do
  factory :review do
    rate 4
    comment 'Muito bom'
    date DateTime.new(2018,07,29)
    product FactoryGirl.build(:product)

    trait :without_product do
      product nil
    end

    trait :rate_not_integer do
      rate 2.3
    end

    trait :rate_greater_than_5 do
      rate 6
    end

    trait :rate_lesser_than_1 do
      rate 0
    end

    trait :without_rate do
      rate nil
    end

    trait :without_date do
      date nil
    end

    trait :without_comment do
      comment nil
    end
  end
end
