require 'rails_helper'

RSpec.describe ProductsController, type: :controller do

  let(:valid_attributes) {
    { title: 'Fogao', description: '4 bocas', price: 1000, 
      manufacturer_id: 1, category_ids: [1] }
  }

  let(:invalid_attributes) {
    FactoryGirl.build(:product, :without_categories).attributes.symbolize_keys
  }

  describe "GET #index" do
    it "returns a success response" do
      get :index, params: {}
      expect(response).to be_success
    end
  end

  describe "GET #show" do
    it "returns a success response" do
      product = FactoryGirl.create(:product)
      get :show, params: {id: product.to_param}
      expect(response).to be_success
    end
  end

  describe "POST #create" do
    context "with valid params" do
      it "creates a new Product" do
        expect {
          post :create, params: {product: valid_attributes}, format: :json
        }.to change(Product, :count).by(1)
      end

      it "renders a JSON response with the new product" do

        post :create, params: {product: valid_attributes}, format: :json
        expect(response).to have_http_status(:created)
        expect(response.content_type).to eq('application/json')
        expect(response.location).to eq(product_url(Product.last))
      end
    end

    context "with invalid params" do
      it "renders a JSON response with errors for the new product" do

        post :create, params: {product: invalid_attributes}, format: :json
        expect(response).to have_http_status(:unprocessable_entity)
        expect(response.content_type).to eq('application/json')
      end
    end
  end

  describe "PUT #update" do
    context "with valid params" do
      let(:new_attributes) {
        FactoryGirl.build(:product, title: 'Fogao editado', price: 2000).attributes.symbolize_keys
      }

      it "updates the requested product" do
        product = FactoryGirl.create(:product)
        put :update, params: {id: product.to_param, product: new_attributes}, format: :json
        product.reload
        expect(product.title).to eq(new_attributes[:title])
      end

      it "renders a JSON response with the product" do
        product = FactoryGirl.create(:product)

        put :update, params: {id: product.to_param, product: valid_attributes}, format: :json
        expect(response).to have_http_status(:ok)
        expect(response.content_type).to eq('application/json')
      end
    end

    context "with invalid params" do
      it "renders a JSON response with errors for the product" do
        product = FactoryGirl.create(:product)

        put :update, params: {id: product.to_param, product: invalid_attributes}, format: :json
        expect(response).to have_http_status(:unprocessable_entity)
        expect(response.content_type).to eq('application/json')
      end
    end
  end

  describe "DELETE #destroy" do
    it "destroys the requested product" do
      product = FactoryGirl.create(:product)
      expect {
        delete :destroy, params: {id: product.to_param}
      }.to change(Product, :count).by(-1)
    end
  end

end
