require "rails_helper"

RSpec.describe ManufacturersController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/manufacturers").to route_to("manufacturers#index")
    end


    it "routes to #show" do
      expect(:get => "/manufacturers/1").to route_to("manufacturers#show", :id => "1")
    end

  end
end
