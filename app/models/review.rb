class Review < ApplicationRecord
  belongs_to :product

  validates :rate, presence: true, numericality: { only_integer: true },
    inclusion: 1..5
  validates :date, presence: true
end
