class CreateReviews < ActiveRecord::Migration[5.1]
  def change
    create_table :reviews do |t|
      t.integer :rate
      t.text :comment
      t.datetime :date

      t.timestamps
    end
  end
end
