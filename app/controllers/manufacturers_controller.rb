class ManufacturersController < ApplicationController
  before_action :set_manufacturer, only: [:show]

  swagger_controller :manufacturers, 'Fabricantes'

  swagger_api :index do
    summary 'Retorna os fabricantes'
    notes 'Este serviço retorna os fabricantes'
  end

  # GET /manufacturers
  def index
    @manufacturers = Manufacturer.all

    render json: @manufacturers
  end

  swagger_api :show do
    summary 'Retorna um Fabricante'
    notes 'Este serviço retorna um Fabricante através do Id'
    param :path, :id, :integer, :required, 'Id do Fabricante'
  end

  # GET /manufacturers/1
  def show
    render json: @manufacturer
  end

  swagger_model :Manufacturer do
    description 'Fabricante'
    property :name, :string, :required, 'Nome'
    property :phone, :string, :required, 'Telefone'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_manufacturer
      @manufacturer = Manufacturer.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def manufacturer_params
      params.require(:manufacturer).permit(:name, :phone)
    end
end
