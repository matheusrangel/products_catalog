FactoryGirl.define do
  factory :manufacturer do
    name 'LG'
    phone '8888-8888'

    trait :without_name do
      name nil
    end

    trait :without_phone do
      phone nil
    end
  end
end
