FactoryGirl.define do
  factory :product do
    title 'Fogão'
    description '4 bocas'
    price 1200
    manufacturer FactoryGirl.build(:manufacturer)
    categories FactoryGirl.build_list(:category, 2)

    trait :without_manufacturer do
      manufacturer nil
    end

    trait :without_categories do
      categories []
    end
  end
end
