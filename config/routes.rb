Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  resources :products do
    resources :reviews, shallow: true
  end

  resources :manufacturers, only: [:index, :show]
  resources :categories, only: [:index, :show]

  get '/docs', to: redirect('/apidocs/index')

end
