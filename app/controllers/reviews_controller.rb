class ReviewsController < ApplicationController
  before_action :set_review, only: [:show, :update, :destroy]

  swagger_controller :reviews, 'Análises'

  swagger_api :index do
    summary 'Retorna análises de um produto'
    notes 'Este serviço busca por análises de produtos'
    param :path, :product_id, :integer, :required, 'Id do Produto'
  end

  # GET /reviews
  def index
    @reviews = Review.where(product_id: params[:product_id])

    render json: @reviews
  end

  swagger_api :show do
    summary 'Retorna uma Análise'
    notes 'Este serviço retorna uma análise através do Id'
    param :path, :id, :integer, :required, 'Id da Análise'
  end

  # GET /reviews/1
  def show
    render json: @review
  end

  swagger_api :create do
    summary 'Cria uma nova Análise'
    notes 'Este serviço cria uma nova Análise de um Produto'
    param :path, :product_id, :integer, :required, 'Id do Produto'
    param :body, :review, :Review, :required, 'objeto Análise'
  end

  # POST /reviews
  def create
    @review = Review.new(review_params)
    @review.product_id = params[:product_id]

    if @review.save
      render json: @review, status: :created, location: @review
    else
      render json: @review.errors, status: :unprocessable_entity
    end
  end

  swagger_api :update do
    summary 'Atualiza uma Análise'
    notes 'Este serviço atualiza uma análise existente com base no seu Id'
    param :path, :id, :integer, :required, 'Id da Análise'
    param :body, :review, :Review, :required, 'objeto Análise'
  end

  # PATCH/PUT /reviews/1
  def update
    if @review.update(review_params)
      render json: @review
    else
      render json: @review.errors, status: :unprocessable_entity
    end
  end

  swagger_api :destroy do
    summary 'Remove uma Análise'
    notes 'Este serviço remove uma análise através do id'
    param :path, :id, :integer, :required, 'Id da Análise'
  end

  # DELETE /reviews/1
  def destroy
    @review.destroy
  end

  swagger_model :Review do
    description 'Análise'
    property :rate, :integer, :required, 'Título'
    property :comment, :string, :required, 'Descrição'
    property :date, :datetime, :required, 'Data'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_review
      @review = Review.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def review_params
      params.require(:review).permit(:rate, :comment, :date)
    end
end
