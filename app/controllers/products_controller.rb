class ProductsController < ApplicationController
  before_action :set_product, only: [:show, :update, :destroy]

  swagger_controller :products, 'Produtos'

  swagger_api :index do
    summary 'Retorna produtos existentes'
    notes 'Este serviço busca por produtos e pode aceitar filtros'
    param :query, :title, :string, :optional, 'Título do Produto'
    param :query, :manufacturer_id, :integer, :optional, 'Id do Fabricante'
  end

  # GET /products
  def index
    @products = Product.where(nil)
    @products = @products.title_contains(params[:title]) if params[:title].present?
    @products = @products.manufacturer(params[:manufacturer_id]) if params[:manufacturer_id].present?
    render json: @products
  end

  swagger_api :show do
    summary 'Retorna um Produto'
    notes 'Este serviço busca por um produto através do id'
    param :path, :id, :integer, :required, 'Id do Produto'
  end

  # GET /products/1
  def show
    render json: @product
  end

  swagger_api :create do
    summary 'Cria um novo Produto'
    notes 'Este serviço cria um novo Produto'
    param :body, :product, :Product, :required, 'objeto Produto'
  end

  # POST /products
  def create
    @product = Product.new(product_params)
    @product.category_ids = params[:category_ids]

    if @product.save
      render json: @product, status: :created, location: @product
    else
      render json: @product.errors, status: :unprocessable_entity
    end
  end

  swagger_api :update do
    summary 'Atualiza um Produto'
    notes 'Este serviço atualiza um produto existente com base no seu Id'
    param :path, :id, :integer, :required, 'Id do Produto'
    param :body, :product, :Product, :required, 'objeto Produto'
  end

  # PATCH/PUT /products/1
  def update
    if @product.update(product_params)
      render json: @product
    else
      render json: @product.errors, status: :unprocessable_entity
    end
  end

  swagger_api :destroy do
    summary 'Remove um Produto'
    notes 'Este serviço remove um produto através do id'
    param :path, :id, :integer, :required, 'Id do Produto'
  end

  # DELETE /products/1
  def destroy
    @product.destroy
  end

  swagger_model :Product do
    description 'Produto'
    property :title, :string, :required, 'Título'
    property :description, :string, :required, 'Descrição'
    property :price, :decimal, :required, 'Preço'
    property :manufacturer_id, :integer, :required, 'Id do Fabricante'
    property :category_ids, :array, :required, 'Ids das Categorias'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_product
      @product = Product.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def product_params
      params.require(:product).permit(
        :title, :description, :price, :manufacturer_id, :category_ids)
    end
end
