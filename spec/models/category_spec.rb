require 'rails_helper'

RSpec.describe Category, type: :model do
  it 'deve ser valido' do
    expect(build(:category)).to be_valid
  end

  it 'deve ser invalido se nao possuir nome' do
    expect(build(:category, :without_name)).not_to be_valid
  end

  it 'deve ser valido se nao possuir descricao' do
    expect(build(:category, :without_description)).to be_valid
  end
end
