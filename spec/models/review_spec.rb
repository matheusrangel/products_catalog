require 'rails_helper'

RSpec.describe Review, type: :model do
  it 'deve ser valido' do
    expect(build(:review)).to be_valid
  end

  it 'deve ser invalido quando nao possuir produto' do
    expect(build(:review, :without_product)).not_to be_valid
  end

  it 'deve ser invalido quando nao possuir data' do
    expect(build(:review, :without_date)).not_to be_valid
  end

  it 'deve ser valido quando nao possuir comentario' do
    expect(build(:review, :without_comment)).to be_valid
  end

  it 'deve ser invalido quando nota for invalida' do
    expect(build(:review, :without_rate)).not_to be_valid
    expect(build(:review, :rate_not_integer)).not_to be_valid
    expect(build(:review, :rate_lesser_than_1)).not_to be_valid
    expect(build(:review, :rate_greater_than_5)).not_to be_valid
  end
end
