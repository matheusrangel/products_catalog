# Catálogo de Produtos

## Configurar e rodar

``` sh
bundle
rake db:create db:migrate db:seed
rails server
```

## Documentação

SwaggerUI através do path:
``` sh
http://localhost:3000/docs
```

## Executar testes

OBS: O teste de controllers não foram finalizados.

``` sh
rspec
```