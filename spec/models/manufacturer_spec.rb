require 'rails_helper'

RSpec.describe Manufacturer, type: :model do
  it 'deve ser valido' do
    expect(build(:manufacturer)).to be_valid
  end

  it 'deve ser invalido se nao possuir nome' do
    expect(build(:manufacturer, :without_name)).not_to be_valid
  end

  it 'deve ser valido se nao possuir telefone' do
    expect(build(:manufacturer, :without_phone)).to be_valid
  end
end
