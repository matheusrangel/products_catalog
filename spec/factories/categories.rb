FactoryGirl.define do
  factory :category do
    name 'Geral'
    description 'Categoria Geral'

    trait :without_name do
      name nil
    end

    trait :without_description do
      description nil
    end
  end
end
